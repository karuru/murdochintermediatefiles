# MurdochIntermediateFiles
Perform operations on WRF intermediate files (eg. read/write). 

We can also interpolate 3D fields from hybrid to pressure levels.

**note**: For now, we only read data that uses the Cylindrical equidistant projection. Function definitions to read the other projections (Mecator, Lambert Conformal, Gaussian or Polar-stereographic  projections) have been setup in the src/record.jl. Thes functions have been commented out for now, as they have have not yet been tested. Check out the [record specifications](https://www2.mmm.ucar.edu/wrf/OnLineTutorial/Basics/IM_files/IM_wps.php) 

## installing the pakage 
```julia

julia > using Pkg
julia > Pkg.add("https://karuru@bitbucket.org/karuru/MurdochIntermediateFiles.git")
```

## Loading package
```julia
julia > using MurdochIntermediateFiles
```

## Example usage
### Read variable from file
```julia
julia > fname = "ACCESS-ESM1-5_20THC:1980-01-01_00"
julia > getvar(fname, "TT")

78×65×39 DimArray{Float32,3} TT with dimensions: 
  X Sampled{Int64} 1:78 ForwardOrdered Regular Points,
  Y Sampled{Int64} 1:65 ForwardOrdered Regular Points,
  Z Sampled{Float32} Float32[38.0, 37.0, …, 1.0, 200100.0] Unordered Irregular Points
[:, :, 1]
       1        2        3        4        5      …   62       63       64       65
  1  277.458  277.563  278.174  278.97   279.045     293.917  297.33   297.95   298.166
  2  274.445  277.836  278.003  278.46   278.858     294.37   294.448  295.188  294.334
  3  274.68   277.809  277.652  278.054  278.433     293.866  294.623  292.838  293.397
  4  274.318  274.295  277.708  277.829  278.02      297.574  293.467  293.646  292.934
  ⋮                                        ⋮      ⋱                               ⋮
 74  276.054  277.376  278.694  280.17   281.73      297.231  296.938  296.647  296.359
 75  275.616  276.565  277.818  279.386  281.022     296.757  296.67   296.306  296.064
 76  275.45   276.19   277.223  278.808  280.452     296.814  296.463  296.105  295.741
 77  275.597  276.267  277.046  278.379  279.748     296.383  296.253  295.866  295.477
 78  275.499  276.483  277.193  278.25   279.393  …  296.328  296.062  295.546  295.349
[and 38 more slices...]
```



### Convert endian order

```julia
# Get and load Packages
julia > using Pkg
julia > Pkg.add("https://karuru@bitbucket.org/karuru/MurdochIntermediateFiles.git")
julia > using MurdochIntermediateFiles

# Get files 
# you can also restrict the files you want by filtering the output from readdir()  
julia > fnames = readdir() # filter(x -> occursin("ACCESS-ESM1", x), readdir()) 

# create directory for new files
julia > !isdir("converted") && mkdir("converted")
julia > for fn in fnames
	
    isfile("converted/" * fn) && rm("converted/" * fn)

    f = FortranFile(fn, convert = "big-endian")
    out_f = FortranFile("converted/" * fn, "w")

    # Read records as defined in https://www2.mmm.ucar.edu/wrf/OnLineTutorial/Basics/IM_files/IM_wps.php
    while !eof(f)
        ifv = read(f, Int32)
        hdate, xfcst, map_source, field, units, desc, xlvl, nx, ny, iproj =
            read(f, FString{24}, Float32, FString{32}, FString{9},
                FString{25}, FString{46}, Float32, Int32, Int32, Int32)

        projrecs = MurdochIntermediateFiles.projectionrec(Val(Int(iproj)), f)
        is_wind_earth_rel = read(f, Int32)
        slab = read(f, (Float32, nx,ny))

        write(out_f, ifv)
        write(out_f, hdate, xfcst, map_source, field, units, desc, xlvl, nx, ny,  iproj)
        write(out_f, projrecs...)
        write(out_f, is_wind_earth_rel)
        write(out_f, slab)
    end
    close(f)
    close(out_f)
end
```

### Interpolate to pressure levels

Define pressure levels that we will interpolate to.
```julia

julia > levs = [100.0, 200.0, 300.0, 500.0, 700.0, 1000.0, 2000.0,
        3000.0, 5000.0, 7000.0,10000.0, 12500.0, 15000.0,
        17500.0, 20000.0, 22500.0, 25000.0, 30000.0, 35000.0,
        40000.0, 45000.0, 50000.0, 55000.0, 60000.0, 65000.0,
        70000.0, 75000.0, 77500.0, 80000.0, 82500.0, 85000.0,
        87500.0, 90000.0, 92500.0, 95000.0, 97500.0, 100000.0]

```

Load data
```julia
julia > f = "3DFILE:1950-01-01_06";
julia > pres = getvar(f, "PRES")
julia > TT = getvar(f, "TT");
julia > VV = getvar(f, "VV");
julia > UU = getvar(f, "UU");
julia > GHT1 = getvar(f, "GHT");
julia > SPECHUMD = getvar(f, "SPECHUMD");
```

Interpolate data
```julia
julia > TT_int = topressure(pres, TT, levs);
julia > VV_int = topressure(pres, VV, levs);
julia > UU_int = topressure(pres, UU, levs);
julia > GHT_int =  topressure(pres, GHT, levs);
julia > SPECHUMD_int = topressure(pres, SPECHUMD, levs);
```

Create levels variable.

**note**: Since we are using [DimensionData.jl](https://rafaqz.github.io/DimensionalData.jl/stable/), we need to make sure that levels is a DimArray.

```julia
julia > levels = Array{Float32}(undef, size(TT_int)...)
julia > for (idx, val) in enumerate(levs)
    levels[:,:,idx] .= val
end
julia > levels = DimArray(levels, (dims(TT_int)), (), :PRES, metadata(pres))
```

Write interpolated data to file
```julia

julia > write("NEW3D:1950-01-01_06",
    (TT_int, VV_int, UU_int, GHT_int, SPECHUMD_int, levels)
    )
```
