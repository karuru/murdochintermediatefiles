"""
    projectionrec(::Val, f)

Returned record based on projection in file (iproj)

See https://www2.mmm.ucar.edu/wrf/OnLineTutorial/Basics/IM_files/IM_wps.php
"""
# Cylindrical equidistant projection
function projectionrec(::Val{0}, f)
    startloc, startlat, startlon, deltalat, deltalon, earth_radius = read(f, FString{8}, Float32, Float32, Float32, Float32, Float32)
    (startloc = startloc, startlat = startlat, startlon = startlon, deltalat = deltalat, deltalon = deltalon, earth_radius = earth_radius)
end
# Mecator projection
# projectionrec(::Val{1}) = read(f, FString{8}, Float32, Float32, Float32, Float32, Float32, Float32)
# #Lambert Conformal projection
# projectionrec(::Val{3}) = read(f, FString{8}, Float32, Float32, Float32, Float32, Float32, Float32, Float32, Float32)
# # Gaussian projection
# projectionrec(::Val{4}) = read(f, FString{8}, Float32, Float32, Float32, Float32, Float32)
# # Polar-stereographic projection
# projectionrec(::Val{5}) = read(f, FString{8}, Float32, Float32, Float32, Float32, Float32)
