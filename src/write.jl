"""
    write(fn::string, T::Tuple)

fn: filename.

T: Tuple of either 3d or 2d DimensionalData Arrays.

Write data to a FortranFile
"""

function Base.write(fn::String, T::Tuple)
    isfile(fn) ? rm(fn) : nothing
    f = FortranFiles.FortranFile(fn, "w"; convert = "native")
    nx = convert(Int32, size(T[1], X))
    ny = convert(Int32, size(T[1], Y))
    xlvl = ndims(T[1]) == 3 ? convert(Array{Float32}, DimensionalData.val(T[1], Z)) : nothing

    for eachlev in axes(T[1], 3)
        for (idx, eachvar) in enumerate(T)
            A = eachvar[:,:, eachlev]

            meta = metadata(A)
            field = FString(9, String(name(A)))
            projrecs = values(meta[:projrecs])

            write(f, meta[:ifv])
            write(f, meta[:hdate], meta[:xfcst], meta[:map_source], field, meta[:units], meta[:desc], xlvl[eachlev], nx, ny,  meta[:iproj])
            write(f, projrecs...)
            write(f, meta[:is_wind_earth_rel])
            write(f, DimensionalData.data(A))
        end
    end
    close(f)
end
