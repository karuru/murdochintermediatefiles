module MurdochIntermediateFiles
using DimensionalData
using Reexport
using FortranFiles
using Interpolations

@reexport using DimensionalData
@reexport using FortranFiles

include("getvar.jl")
export getvar

include("record.jl")
export topressure

include("interpolationroutines.jl")
include("write.jl")

end # module
