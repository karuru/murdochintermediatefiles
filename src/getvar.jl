"""
        getvar(fn, vname; convert = "native")

Get variable `vname` from a WRF intermediate file `fn`.

If required use `convert` to read the WRF interemdiate file in the correct byte order. ie. options are `big-endian` or `little-endian`

## Example 
Read TT data from file
```julia
julia > getvar("INTFILE:1980-01-01_00", "TT")
```
Specify byte order
```julia
julia > getvar("INTFILE:1980-01-01_00", "TT"; convert = "big-endian")
```
"""
function getvar(fn, vname; convert = "native")
    f = FortranFile(fn, convert = convert)

    # We need to define these variables due to scoping within the while loop
    slab, ifv, hdate, xfcst, map_source, field, units, desc,
    xlvl, nx, ny, iproj, projrecs, is_wind_earth_rel, meta = [nothing for _ in 1:15]

    levels = Float32[]

    while !eof(f)
        ifv = read(f, Int32)
        hdate, xfcst, map_source, field, units, desc, xlvl, nx, ny, iproj =
            read(f, FString{24}, Float32, FString{32}, FString{9},
                FString{25}, FString{46}, Float32, Int32, Int32, Int32)

        projrecs = projectionrec(Val(Int(iproj)), f)
        is_wind_earth_rel = read(f, Int32)

        if String(trim(field)) == vname
            push!(levels, xlvl)
            if isnothing(slab)
                slab = read(f, (Float32, nx,ny))
            else
                slab = cat(slab, read(f, (Float32, nx,ny)), dims = 3)
            end
            meta = (ifv = ifv, hdate = hdate, xfcst = xfcst, map_source = map_source,
                units = units, desc = desc, iproj = iproj, projrecs = projrecs,
                is_wind_earth_rel = is_wind_earth_rel)
        else
            read(f)
        end
    end
    close(f)

    # dimensions = ndims(slab) == 2 ? (X(1:nx), Y(1:ny)) : (X(1:nx), Y(1:ny), Z(levels))
    dimensions = (X(1:size(slab, 1)), Y(1:size(slab,2)), Z(levels))
    slab = ndims(slab) == 2 ? reshape(slab, (size(slab)...,1)) : slab
    DimArray(slab, DimensionalData.format(dimensions, slab), (), Symbol(vname), meta)
end

