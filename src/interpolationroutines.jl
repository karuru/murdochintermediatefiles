"""
    topressure(pres, A, levs)

pres: Current pressure levels in file.

A: Data to be interolated.

levs: levels that we want to interpolate to.
"""
function topressure(pres, A, levs)
    nx = size(A, X)
    ny = size(A, Y)
    nz = length(levs)
    slab = Array{eltype(A)}(undef, nx, ny, nz)
    for j in Base.OneTo(ny)
        for i in Base.OneTo(nx)
            itp = interpolate((pres[i,j, end:-1:1],), A[i,j, end:-1:1],  Gridded(Linear()))
            etp = extrapolate(itp, Line())
            T = etp(levs)
            for z in Base.OneTo(nz)
                slab[i, j, z] = T[z]
            end
        end
    end
    rebuild(A, slab, DimensionalData.format((X(1:nx), Y(1:ny), Z(levs)), slab))
end
